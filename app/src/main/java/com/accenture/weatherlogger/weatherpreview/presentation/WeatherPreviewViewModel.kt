package com.accenture.weatherlogger.weatherpreview.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.accenture.weatherlogger.base.BaseViewModel
import com.accenture.weatherlogger.database.WeatherState
import com.accenture.weatherlogger.extension.SingleLiveEvent
import com.accenture.weatherlogger.weatherpreview.data.WeatherRepository
import com.accenture.weatherlogger.weatherpreview.data.model.toWeatherState
import com.accenture.weatherlogger.weatherpreview.presentation.model.WeatherStateUIModel
import com.accenture.weatherlogger.weatherpreview.presentation.model.toUiModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class WeatherPreviewViewModel(private val weatherRepository: WeatherRepository) : BaseViewModel() {

    val weatherItemClickEvent = SingleLiveEvent<WeatherStateUIModel>()

    val locationPermissionRequestEvent = SingleLiveEvent<Any>()

    val weatherStateSaveSuccessEvent = SingleLiveEvent<Any>()

    val weatherStateSaveErrorEvent = SingleLiveEvent<Any>()

    private val _weatherStateData = MutableLiveData<List<WeatherStateUIModel>>()
    val weatherStateData: LiveData<List<WeatherStateUIModel>>
        get() = _weatherStateData

    fun onSaveLocationClicked() {
        locationPermissionRequestEvent.call()
    }

    fun getWeatherStates() {
        weatherRepository
            .getAllWeatherStates()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = { weatherStates ->
                    _weatherStateData.value = weatherStates.map { it.toUiModel() }
                },
                onError = { throwable ->
                    consumeError(throwable)
                }
            ).addToDisposableBag()
    }

    fun onWeatherItemClicked(weatherItem: WeatherStateUIModel) {
        weatherItemClickEvent.value = weatherItem
    }

    fun onLocationInformationFetched(latitude: Double, longitude: Double) {
        weatherRepository
            .getCurrentWeatherInformation(latitude, longitude)
            .applyLoadingState()
            .subscribeBy(
                onSuccess = { weatherDetails ->
                    saveWeatherState(weatherDetails.toWeatherState())
                },
                onError = { throwable ->
                    consumeError(throwable)
                }
            ).addToDisposableBag()
    }

    private fun saveWeatherState(state: WeatherState) {
        weatherRepository.insertWeatherState(state)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onComplete = {
                    weatherStateSaveSuccessEvent.call()
                }, onError = {
                    weatherStateSaveErrorEvent.call()
                }).addToDisposableBag()
    }
}
