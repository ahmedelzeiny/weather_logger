package com.accenture.weatherlogger.weatherpreview.data.model

import android.os.Parcelable
import com.accenture.weatherlogger.database.WeatherState
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.Date

@Parcelize
data class WeatherDetails(
    @SerializedName("base")
    val base: String?,
    @SerializedName("clouds")
    val clouds: Clouds?,
    @SerializedName("cod")
    val cod: Int?,
    @SerializedName("coord")
    val coord: Coord?,
    @SerializedName("dt")
    val dt: Int?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("main")
    val main: Main?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("sys")
    val sys: Sys?,
    @SerializedName("weather")
    val weather: List<Weather?>?,
    @SerializedName("wind")
    val wind: Wind?
) : Parcelable

fun WeatherDetails.toWeatherState() = WeatherState(
    savedDate = Date(),
    countryName = sys?.country.orEmpty(),
    locationName = name.orEmpty(),
    latitude = coord?.lat ?: 0.0,
    longitude = coord?.lon ?: 0.0,
    weatherDescription = weather?.get(0)?.description.orEmpty(),
    currentTemp = main?.temp ?: 0.0
)
