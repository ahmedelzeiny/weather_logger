package com.accenture.weatherlogger.weatherpreview.presentation

import android.Manifest
import android.location.Location
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import com.accenture.weatherlogger.R
import com.accenture.weatherlogger.base.BindingActivity
import com.accenture.weatherlogger.databinding.ActivityWeatherPreviewBinding
import com.accenture.weatherlogger.extension.showGpsDisabledAlert
import com.accenture.weatherlogger.extension.toast.ToastState
import com.accenture.weatherlogger.extension.toast.showToast
import com.accenture.weatherlogger.weatherpreview.presentation.adapter.WeatherStateClickListener
import com.accenture.weatherlogger.weatherpreview.presentation.adapter.WeatherStatesAdapter
import com.google.android.gms.location.LocationServices
import com.tbruyelle.rxpermissions2.RxPermissions
import org.koin.android.ext.android.inject

class WeatherPreviewActivity : BindingActivity<ActivityWeatherPreviewBinding>() {
    private val rxPermissions by lazy {
        RxPermissions(this)
    }

    private val fusedLocationClient by lazy {
        LocationServices.getFusedLocationProviderClient(this)
    }

    private val weatherStateAdapter by lazy {
        WeatherStatesAdapter(WeatherStateClickListener { weatherItem ->
            viewModel.onWeatherItemClicked(weatherItem)
        })
    }

    override val viewModel: WeatherPreviewViewModel by inject()

    override fun getLayoutResId(): Int = R.layout.activity_weather_preview

    override fun onViewBound(binding: ActivityWeatherPreviewBinding) {
        setupToolbar()
        setupLocationPermissionObserver()
        setupWeatherStatesObserver()
        setupWeatherList()
        setupWeatherItemClickObserver()
        setupSaveWeatherStateObserver()
        viewModel.getWeatherStates()
    }

    private fun setupToolbar() {
        setSupportActionBar(binding.weatherLoggerToolbar)
        supportActionBar?.run {
            title =
                getString(R.string.app_name)
            setDisplayShowTitleEnabled(true)
        }
    }

    private fun setupSaveWeatherStateObserver() {
        viewModel.weatherStateSaveErrorEvent.observe(this, Observer {
            applicationContext.showToast(
                ToastState.Error,
                getString(R.string.failed_to_load_weather_information)
            )
        })

        viewModel.weatherStateSaveSuccessEvent.observe(this, Observer {
            applicationContext.showToast(
                ToastState.Success,
                getString(R.string.weather_information_saved_successfully)
            )
        })
    }

    private fun setupWeatherList() {
        binding.weatherList.adapter = weatherStateAdapter
    }

    private fun setupWeatherStatesObserver() {
        viewModel.weatherStateData.observe(this, Observer { weatherStates ->
            weatherStateAdapter.submitList(weatherStates)
        })
    }

    private fun setupLocationPermissionObserver() {
        viewModel.locationPermissionRequestEvent.observe(this, Observer {
            requestLocationPermission()
        })
    }

    private fun setupWeatherItemClickObserver() {
        viewModel.weatherItemClickEvent.observe(this, Observer { weatherState ->
            applicationContext.showToast(
                ToastState.Success,
                weatherState.toString()
            )
        })
    }

    private fun requestLocationPermission() {
        rxPermissions
            .request(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            .subscribe { granted ->
                when (granted) {
                    granted == true -> getLastKnownLocation()
                    else -> showGpsDisabledAlert()
                }
            }.dispose()
    }

    private fun getLastKnownLocation() {
        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
            location?.let {
                viewModel.onLocationInformationFetched(location.latitude, location.longitude)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.action_save) {
            viewModel.onSaveLocationClicked()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
