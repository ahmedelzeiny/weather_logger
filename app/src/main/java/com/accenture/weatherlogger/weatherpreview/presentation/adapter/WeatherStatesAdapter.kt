package com.accenture.weatherlogger.weatherpreview.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.accenture.weatherlogger.databinding.WeatherItemViewBinding
import com.accenture.weatherlogger.extension.inflater
import com.accenture.weatherlogger.extension.toDefaultFormat
import com.accenture.weatherlogger.weatherpreview.presentation.model.WeatherStateUIModel

class WeatherStatesAdapter(private val clickListener: WeatherStateClickListener) :
    ListAdapter<WeatherStateUIModel, WeatherStatesAdapter.ViewHolder>(
        WeatherStateDiffCallback()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let {
            holder.bind(it, position, clickListener)
        }
    }

    class ViewHolder private constructor(val binding: WeatherItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: WeatherStateUIModel,
            position: Int,
            clickListener: WeatherStateClickListener
        ) {
            binding.position = position
            binding.clickListener = clickListener
            binding.item = item
            binding.dateValueLabel.text = item.savedDate?.toDefaultFormat()
            binding.temperatureValueLabel.text = item.currentTemp.toString()
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = parent.context.inflater
                val binding =
                    WeatherItemViewBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }
}

class WeatherStateClickListener(val clickListener: (item: WeatherStateUIModel) -> Unit) {
    fun onItemClicked(item: WeatherStateUIModel) = clickListener(item)
}

class WeatherStateDiffCallback : DiffUtil.ItemCallback<WeatherStateUIModel>() {
    override fun areItemsTheSame(
        oldItem: WeatherStateUIModel,
        newItem: WeatherStateUIModel
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: WeatherStateUIModel,
        newItem: WeatherStateUIModel
    ): Boolean {
        return oldItem == newItem
    }
}
