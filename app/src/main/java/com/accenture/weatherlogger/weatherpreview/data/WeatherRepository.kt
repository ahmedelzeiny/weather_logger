package com.accenture.weatherlogger.weatherpreview.data

import com.accenture.weatherlogger.database.WeatherDAO
import com.accenture.weatherlogger.database.WeatherState

class WeatherRepository(
    private val weatherDAO: WeatherDAO,
    private val weatherEndpoint: WeatherEndpoint
) {

    fun getAllWeatherStates() = weatherDAO.getAllWeatherStates()

    fun insertWeatherState(state: WeatherState) = weatherDAO.insertWeatherState(state)

    fun deleteAllWeatherStates() = weatherDAO.deleteAllWeatherStates()

    fun updateWeatherState(state: WeatherState) = weatherDAO.updateWeatherState(state)

    fun deleteWeatherState(state: WeatherState) = weatherDAO.deleteWeatherState(state)

    fun getWeatherStateById(id: Int) = weatherDAO.getWeatherStateById(id)

    fun getCurrentWeatherInformation(latitude: Double, longitude: Double) =
        weatherEndpoint.fetchWeatherDetails(latitude, longitude)
}
