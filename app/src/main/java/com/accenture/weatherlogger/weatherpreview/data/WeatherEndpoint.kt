package com.accenture.weatherlogger.weatherpreview.data

import com.accenture.weatherlogger.BuildConfig
import com.accenture.weatherlogger.weatherpreview.data.model.WeatherDetails
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherEndpoint {
    @GET("weather")
    fun fetchWeatherDetails(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("appid") apiKey: String = BuildConfig.WEATHER_API_KEY
    ): Single<WeatherDetails>
}
