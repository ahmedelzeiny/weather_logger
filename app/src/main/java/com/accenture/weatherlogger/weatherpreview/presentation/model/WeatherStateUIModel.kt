package com.accenture.weatherlogger.weatherpreview.presentation.model

import com.accenture.weatherlogger.database.WeatherState
import java.util.Date

data class WeatherStateUIModel(
    val id: Int,
    val savedDate: Date?,
    val countryName: String,
    val locationName: String,
    val latitude: Double,
    val longitude: Double,
    val weatherDescription: String,
    val currentTemp: Double
)

fun WeatherState.toUiModel() = WeatherStateUIModel(
    id,
    savedDate,
    countryName,
    locationName,
    latitude,
    longitude,
    weatherDescription,
    currentTemp
)
