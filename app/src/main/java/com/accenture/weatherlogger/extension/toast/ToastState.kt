package com.accenture.weatherlogger.extension.toast

enum class ToastState {
    Success, Error
}
