package com.accenture.weatherlogger.extension

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.provider.Settings
import android.view.LayoutInflater
import android.widget.Button
import com.accenture.weatherlogger.R

val Context.inflater: LayoutInflater
    get() = LayoutInflater.from(this)

fun Context.showGpsDisabledAlert() {
    val view = inflater.inflate(R.layout.dialog_ask_to_enable_gps, null)
    val ok = view.findViewById<Button>(R.id.ok)
    val cancel = view.findViewById<Button>(R.id.cancel)
    val dialog = AlertDialog.Builder(this)
        .setView(view)
        .create()

    ok.setOnClickListener {
        dialog.dismiss()
        val gpsSettingIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        startActivity(gpsSettingIntent)
    }
    cancel.setOnClickListener {
        dialog.dismiss()
    }
    dialog.show()
}
