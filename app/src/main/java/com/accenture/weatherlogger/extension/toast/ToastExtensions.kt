package com.accenture.weatherlogger.extension.toast

import android.content.Context
import android.view.Gravity
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.accenture.weatherlogger.R
import com.accenture.weatherlogger.extension.inflater

fun Context.showToast(type: ToastState, message: String, duration: Int = Toast.LENGTH_LONG) {
    val toast = Toast(this)
    val view = inflater.inflate(R.layout.weather_logger_toast_view, null)
    val textView: TextView = view.findViewById(R.id.toast_label)
    textView.text = message
    if (type == ToastState.Error) {
        textView.background =
            ContextCompat.getDrawable(this, R.drawable.error_toast_background)
    } else {
        textView.background =
            ContextCompat.getDrawable(this, R.drawable.success_toast_background)
    }
    toast.run {
        this.view = view
        this.duration = duration
        this.setGravity(Gravity.TOP or Gravity.FILL_HORIZONTAL,
            GRAVITY_Y_OFFSET,
            GRAVITY_X_OFFSET
        )
        this.show()
    }
}

fun Fragment.showToast(type: ToastState, message: String, duration: Int = Toast.LENGTH_LONG) {
    activity?.showToast(type, message, duration)
}

const val GRAVITY_Y_OFFSET = 40
const val GRAVITY_X_OFFSET = 0
