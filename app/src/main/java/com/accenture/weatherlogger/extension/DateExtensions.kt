package com.accenture.weatherlogger.extension

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

fun Date.toDefaultFormat(): String {
    return SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault()).format(this)
}
