package com.accenture.weatherlogger.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface WeatherDAO {
    @Query("select * from $WEATHER_STATE_TABLE_NAME")
    fun getAllWeatherStates(): Flowable<List<WeatherState>>

    @Query("select * from $WEATHER_STATE_TABLE_NAME where $WEATHER_STATE_TABLE_ID in (:id)")
    fun getWeatherStateById(id: Int): Single<WeatherState>

    @Query("delete from $WEATHER_STATE_TABLE_NAME")
    fun deleteAllWeatherStates(): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWeatherState(weatherState: WeatherState): Completable

    @Update
    fun updateWeatherState(weatherState: WeatherState): Completable

    @Delete
    fun deleteWeatherState(weatherState: WeatherState): Completable
}
