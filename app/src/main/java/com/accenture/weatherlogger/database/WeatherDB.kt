package com.accenture.weatherlogger.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [WeatherState::class], version = 1, exportSchema = false)
@TypeConverters(DateTypeConverter::class)
abstract class WeatherDB : RoomDatabase() {
    abstract fun weatherDAO(): WeatherDAO
}
