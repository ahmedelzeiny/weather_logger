package com.accenture.weatherlogger.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Date

@Entity(tableName = WEATHER_STATE_TABLE_NAME)
data class WeatherState(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = WEATHER_STATE_TABLE_ID)
    val id: Int = 0,
    @ColumnInfo(name = "saved_date")
    val savedDate: Date?,
    @ColumnInfo(name = "country_name")
    val countryName: String,
    @ColumnInfo(name = "location_name")
    val locationName: String,
    @ColumnInfo(name = "latitude")
    val latitude: Double,
    @ColumnInfo(name = "longitude")
    val longitude: Double,
    @ColumnInfo(name = "weather_description")
    val weatherDescription: String,
    @ColumnInfo(name = "current_temp")
    val currentTemp: Double
)

const val WEATHER_STATE_TABLE_NAME = "weather_state"
const val WEATHER_STATE_TABLE_ID = "id"
