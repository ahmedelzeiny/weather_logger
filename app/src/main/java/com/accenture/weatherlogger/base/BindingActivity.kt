package com.accenture.weatherlogger.base

import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BindingActivity<Binding : ViewDataBinding> : BaseActivity() {

    protected lateinit var binding: Binding

    override fun inflateContentView(): View {
        binding = DataBindingUtil.inflate(layoutInflater, getLayoutResId(), null, false)
        return binding.root
    }

    override fun onViewCreated(containerView: ConstraintLayout) {
        onViewBound(binding)
    }

    abstract fun onViewBound(binding: Binding)
}
