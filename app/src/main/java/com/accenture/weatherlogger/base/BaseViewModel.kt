package com.accenture.weatherlogger.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.accenture.weatherlogger.errorhandling.ErrorViewState
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel : ViewModel() {

    private val compositeDisposable = CompositeDisposable()

    private val _loadingState = MutableLiveData<Boolean>()
    val loadingState: LiveData<Boolean>
        get() = _loadingState

    private val _errorViewState = MutableLiveData<ErrorViewState>()
    val errorViewState: LiveData<ErrorViewState>
        get() = _errorViewState

    protected fun consumeError(throwable: Throwable) {
        // TODO add more error handling
        _errorViewState.value = ErrorViewState.Toast(throwable.message.orEmpty())
    }

    protected fun showLoading() {
        _loadingState.value = true
    }

    protected fun hideLoading() {
        _loadingState.value = false
    }

    override fun onCleared() {
        compositeDisposable.dispose()
    }

    protected fun Disposable.addToDisposableBag() {
        compositeDisposable.add(this)
    }

    // Reactive UI extensions
    protected fun <T> Single<T>.applyLoadingState(): Single<T> =
        this.observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { showLoading() }
            .doAfterTerminate { hideLoading() }

    protected fun Completable.applyLoadingState(): Completable =
        this.observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { showLoading() }
            .doAfterTerminate { hideLoading() }

    protected fun <T> Maybe<T>.applyLoadingState(): Maybe<T> =
        this.observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { showLoading() }
            .doAfterTerminate { hideLoading() }
}
