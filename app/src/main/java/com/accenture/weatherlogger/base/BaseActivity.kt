package com.accenture.weatherlogger.base

import android.os.Bundle
import android.view.View
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.accenture.weatherlogger.errorhandling.ErrorViewState
import com.accenture.weatherlogger.extension.addLoadingView
import com.accenture.weatherlogger.extension.getOrWrapConstraintLayout
import com.accenture.weatherlogger.extension.toast.ToastState
import com.accenture.weatherlogger.extension.toast.showToast
import com.accenture.weatherlogger.widget.LoadingView

abstract class BaseActivity : AppCompatActivity() {

    protected abstract val viewModel: BaseViewModel
    protected lateinit var containerView: ConstraintLayout

    private val loadingView by lazy {
        provideLoadingView().also { containerView.addLoadingView(it) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val contentView = inflateContentView()
        containerView = contentView.getOrWrapConstraintLayout()
        setupLoadingView()
        observeOnErrorState()

        setContentView(containerView)
        onViewCreated(containerView)
    }

    private fun inflateContainerView(contentView: View): ConstraintLayout {
        return contentView.getOrWrapConstraintLayout()
    }

    protected open fun inflateContentView(): View {
        return layoutInflater
            .inflate(getLayoutResId(), null, false)
    }

    private fun setupLoadingView() {
        viewModel.loadingState.observe(this, Observer {
            loadingView.isVisible = it
        })
    }

    protected open fun provideLoadingView(): View {
        return LoadingView(this)
    }

    private fun observeOnErrorState() {
        viewModel.errorViewState.observe(this, Observer {
            when (it) {
                is ErrorViewState.Toast -> {
                    showToast(ToastState.Error, it.message)
                }
            }
        })
    }

    @LayoutRes
    abstract fun getLayoutResId(): Int

    abstract fun onViewCreated(containerView: ConstraintLayout)
}
