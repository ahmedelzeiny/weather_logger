package com.accenture.weatherlogger.di

import com.accenture.weatherlogger.network.RetrofitClient
import com.accenture.weatherlogger.weatherpreview.data.WeatherEndpoint
import com.accenture.weatherlogger.weatherpreview.data.WeatherRepository
import com.accenture.weatherlogger.weatherpreview.presentation.WeatherPreviewViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val weatherPreviewModule = module {
    single { get<RetrofitClient>().create(WeatherEndpoint::class.java) }
    factory { WeatherRepository(get(), get()) }
    viewModel { WeatherPreviewViewModel(get()) }
}
