package com.accenture.weatherlogger.di

import androidx.room.Room
import com.accenture.weatherlogger.database.WeatherDB
import org.koin.dsl.module

val databaseModule = module {

    single {
        Room.databaseBuilder(get(), WeatherDB::class.java, "weather-db")
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
    }

    single { get<WeatherDB>().weatherDAO() }
}
