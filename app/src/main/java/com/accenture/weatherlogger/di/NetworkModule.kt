package com.accenture.weatherlogger.di

import com.accenture.weatherlogger.BuildConfig
import com.accenture.weatherlogger.network.HeadersInterceptor
import com.accenture.weatherlogger.network.RetrofitClient
import io.reactivex.schedulers.Schedulers
import org.koin.dsl.module
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

val networkModule = module {
    single {
        RetrofitClient.Builder(BuildConfig.BASE_URL)
            .build {
                useDefaultLoggerInterceptor()
                addInterceptor(HeadersInterceptor())
                addCallAdapter(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            }
    }
}
