package com.accenture.weatherlogger.di

val weatherLoggerModules = listOf(
    networkModule,
    databaseModule,
    weatherPreviewModule
)
