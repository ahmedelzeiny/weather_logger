package com.accenture.weatherlogger.network

import okhttp3.Interceptor
import okhttp3.Response

class HeadersInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val requestBuilder = original.newBuilder()

        requestBuilder.header("Accept-Language", "en")
        requestBuilder.header("Accept", "application/json")
        requestBuilder.header("Content-Type", "application/json")
        requestBuilder.method(original.method, original.body)
        val request = requestBuilder.build()

        return chain.proceed(request)
    }
}
