package com.accenture.weatherlogger

import android.app.Application
import com.accenture.weatherlogger.di.weatherLoggerModules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class WeatherLoggerApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@WeatherLoggerApp)
            modules(weatherLoggerModules)
        }
    }
}
