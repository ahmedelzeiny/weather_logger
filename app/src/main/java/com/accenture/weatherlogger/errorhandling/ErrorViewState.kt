package com.accenture.weatherlogger.errorhandling

sealed class ErrorViewState(val message: String) {

    class Toast(message: String = "") : ErrorViewState(message)
}
