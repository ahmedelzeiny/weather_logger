package com.accenture.weatherlogger.weatherpreview.presentation

import com.accenture.weatherlogger.database.WeatherState
import com.accenture.weatherlogger.weatherpreview.data.model.Weather
import com.accenture.weatherlogger.weatherpreview.data.model.WeatherDetails
import java.util.Date

object WeatherPreviewViewModelDataUtil {
    val weatherState = WeatherState(
        savedDate = Date(),
        countryName = "EG",
        locationName = "Giza",
        latitude = 23.34,
        longitude = 32.23,
        weatherDescription = "Cloud",
        currentTemp = 233.3
    )

    val weatherStateList = listOf(weatherState, weatherState)

    val weather = Weather(
        "description",
        "icon",
        34,
        "main"
    )

    val weatherDetails = WeatherDetails(
        "base",
        null,
        23,
        null,
        23,
        3,
        null,
        "name",
        null,
        listOf(weather),
        null
    )
}
