package com.accenture.weatherlogger.weatherpreview.presentation

import androidx.lifecycle.Observer
import com.accenture.weatherlogger.InstantExecutorExtension
import com.accenture.weatherlogger.TestSchedulerExtension
import com.accenture.weatherlogger.any
import com.accenture.weatherlogger.database.WeatherState
import com.accenture.weatherlogger.weatherpreview.data.WeatherRepository
import com.accenture.weatherlogger.weatherpreview.presentation.model.toUiModel
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.junit.jupiter.MockitoExtension

@ExtendWith(
    InstantExecutorExtension::class,
    MockitoExtension::class,
    TestSchedulerExtension::class
)
class WeatherPreviewViewModelTest {

    private lateinit var viewModel: WeatherPreviewViewModel

    @Mock
    private lateinit var liveEventObserver: Observer<Any>

    @Mock
    private lateinit var weatherRepository: WeatherRepository

    @BeforeEach
    fun setup() {
        viewModel = WeatherPreviewViewModel(weatherRepository)
    }

    @Test
    fun `when getWeatherState called the viewModel the weather state list data should be fetched`() {
        `when`(
            weatherRepository.getAllWeatherStates()
        ).thenReturn(Flowable.just(WeatherPreviewViewModelDataUtil.weatherStateList))
        viewModel.getWeatherStates()
        verify(weatherRepository, times(1)).getAllWeatherStates()
        assertEquals(WeatherPreviewViewModelDataUtil.weatherStateList.map { it.toUiModel() }
            , viewModel.weatherStateData.value)
    }

    @Test
    fun `when save location clicked the location permission event should be called`() {
        viewModel.onSaveLocationClicked()
        viewModel.locationPermissionRequestEvent.observeForever(liveEventObserver)
        verify(liveEventObserver, times(1)).onChanged(null)
        Mockito.verifyNoMoreInteractions(liveEventObserver)
        viewModel.locationPermissionRequestEvent.removeObserver(liveEventObserver)
    }

    @Test
    fun `when location information fetched the weather information should be fetched and saved`() {
        `when`(
            weatherRepository.getCurrentWeatherInformation(
                Mockito.anyDouble(),
                Mockito.anyDouble()
            )
        ).thenReturn(Single.just(WeatherPreviewViewModelDataUtil.weatherDetails))

        `when`(
            weatherRepository.insertWeatherState(any<WeatherState>())
        ).thenReturn(Completable.complete())

        viewModel.onLocationInformationFetched(34.3, 23.23)

        verify(weatherRepository, times(1)).getCurrentWeatherInformation(
            Mockito.anyDouble(),
            Mockito.anyDouble()
        )
    }
}
