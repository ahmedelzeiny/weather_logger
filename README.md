# weather-logger

Sample application to get your current weather temperature using `openweathermap.org`

## Dependencies

* `RxPermissions` - Used to get run time permissions ( Location)
* `play-services-location` - Used to get location updates
* `airbnb.android:lottie` - Show animated loading view
* `room` - Cache weather state offline
* `retrofit2` - Used for Http calls
* `koin` - Service locator lib to help locate and inject dependencies